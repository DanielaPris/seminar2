let dictionary=['the','quick','brown','fox','jumps','over','the','lazy','dog'];

let sampleText=`
best
read
on
windy
nights
`;

let checkAcrostich=(text)=>{
    let candidate=text.split('\n').map((e)=>e.trim()).filter((e)=>e).map((e)=>e[0]).join(' ');
    return dictionary.indexOf(candidate)!==-1
}
console.log(checkAcrostich(sampleText));
