let sampleN=11;
let checkPrime=(n)=>{
    let isPrime=true;
    for(let i=2;i<Math.sqrt(n);i++){
        if(!(n%i)){
            return false;
        }
    }
    return true;
}
console.log(`${sampleN} is ${checkPrime(sampleN) ? 'prime':'not prime'}`);